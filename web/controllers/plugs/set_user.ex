defmodule Discuss.Plugs.SetUser do
	import Plug.Conn
	import Phoenix.Controller

	alias Discuss.Repo
	alias Discuss.User

	def init(_params) do
	end

	# the second argument _params is NOT coming from the request struct/form data
	# _params in this case is the return value of init function of this module
	def call(conn, _params) do
		user_id = get_session(conn, :user_id)

		# condition block returns when a contidion is true
		cond do
			# assigns second arg of && to user
			user = user_id && Repo.get(User, user_id) ->
				assign(conn, :user, user)
				# now this can be accessed as conn.assigns.user ==> user struct
			true ->
				assign(conn, :user, nil)
		end
	end
end