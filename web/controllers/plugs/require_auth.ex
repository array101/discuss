defmodule Discuss.Plugs.RequireAuth do
	import Plug.Conn # halt() function
	import Phoenix.Controller

	alias Discuss.Router.Helpers # Helpers.topic_path

	def init(_params) do
	end

	def call(conn, _params) do
		if conn.assigns[:user] do
			conn
		else
			conn
			|> put_flash(:error, "You must be logged in.")
			|> redirect(to: Helpers.topic_path(conn, :index))
			|> halt()
			# halt() sends back the conn object to the user, no further traversing
		end
	end
end